<h1 align="center">VH-S</h1>
<h3 align="center">Uma ferramenta leve, simples e intuitiva</h3>

<p align="center">
  <a href="#sobre-o-projeto">Sobre o projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#começando">Começando</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#como-contribuir">Como contribuir</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#licença">Licença</a>
</p>

## 💡 Sobre o projeto

Esta aplicação é uma ferramente de lado cliente que possibilita a captura de tela com ou sem áudio para projetos simples de gravação.

- Leve
- Gratuito
- Fácil

Veja o projeto funcionando [aqui](https://jrrsouzaj.codeberg.page/vh-s/@main/).

## Recursos ##

- Permite capturar toda a tela, uma janela ou uma guia
- Permite gravar a partir de uma webcam
- Permite capturar áudio via microfone
- Exporta a gravação

## 🚀 Começando

Se você instalou o git você pode clonar o código para sua máquina, ou baixar um ZIP de todos os arquivos diretamente.
[Faça o download do ZIP deste local](https://codeberg.org/JRRSouzaJ/vh-s/archive/main.zip), ou execute o seguinte comando [git](https://git-scm.com/downloads) para clonar os arquivos em sua máquina:
```bash
https://codeberg.org/JRRSouzaJ/vh-s.git
```
- Assim que os arquivos estiverem em sua máquina, abra a pasta "sobre-mim" no Visual Studio Code. [Visual Studio Code](https://code.visualstudio.com/).
- Com os arquivos abertos em Visual Studio Code, pressione o botão *Go Live* na parte inferior da janela para iniciar os arquivos com o [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer).
- Mude a imagem do perfil e os textos no arquivo `index.html`.
- Mude as cores e fontes no arquivo `style.css`.
- Para mudar o fundo, vá para o arquivo `style.css` na linha 17, descomente o trecho do código e mude a url para a imagem que quiser.

Uma vez que seu pedido de pull tenha sido fundido (merge), você pode apagar sua branch.

## 🤔 Como contribuir ##

- Faça um fork deste repositório;
- Crie uma branch com seus recursos: `git checkout -b my-feature`;
- Confirme suas mudanças: `git commit -m "feat: my new feature"`;
- Envie para sua branch: `git push origem my-feature`.

## Tecnologias ##

Este projeto usa as segintes tecnologias:

- [HTML5] - Para estrutura do site
- [CSS] - Para estilo visual
- [JavaScript] - Para controle do fluxo de dados
- [JQuery] - Para manipulação do DOM

## 📝 License ##

2022 - Futura Criativa

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>.


[//]: # (Referências utilizadas)

   [jQuery]: <http://jquery.com>
   [JavaScript]: <https://www.javascript.com>
   [HTML5]: <https://html.spec.whatwg.org/>
   [CSS]: <https://www.w3.org/Style/CSS>
